
exception Not_same_disc
let gcd_ext a b c =let (a_1, b_1,c_1) =Z.gcdext a b in
let (a2, b2, c2) = Z.gcdext a_1 c in
(a2, Z.mul b2 b_1,Z.mul b2 c_1, c2);;
let add l = List.fold_left Z.add Z.zero l
let mul l = List.fold_left Z.mul Z.one l
let square a=Z.mul a a
let transform (a,b,c) (alpha, beta,gamma, delta) =
  (add [mul [a; alpha;  alpha] ; mul [b;alpha;gamma];  mul [c; gamma; gamma]], 
  add [mul [Z.of_int 2; a; alpha;beta]; mul [b; alpha; delta]; mul [b; beta; gamma]], 
  add [mul [Z.of_int 2;c;gamma; delta]; mul [a; beta; beta]; mul [b; beta; delta]; mul [c; delta; delta]])
let rec reduce (a,b,c) =
  match (Z.lt (Z.neg a) b, Z.leq b a, Z.leq a c ) with
  | (true, true, true) -> if (Z.equal a c) then (a, Z.abs b, c) else (a,b,c)
  | (_, _, false) -> reduce (c, Z.neg b, a)
  | (_, false, _) | (false, _, _) ->
    let (q,r) = Z.div_rem b (Z.shift_left a 1) in
    let q= if (Z.gt r a) then (Z.succ q) else if (Z.leq r (Z.neg a)) then (Z.pred q) else q  in
    let (a,b,c) = transform (a,b,c) (Z.one, Z.neg q, Z.zero, Z.one) in
    reduce (a,b,c)

let identity d =
  if (Z.congruent d Z.one (Z.of_int 4)) 
    then (Z.one, Z.neg Z.one, (Z.shift_right (Z.succ (Z.neg d)) 2)) 
  else (Z.one, Z.zero, (Z.shift_right d 2) )

let disc (a,b,c) =Z.sub (Z.mul b b) (Z.shift_left (Z.mul a c) 2)
let mult (a1,b1,c1) (a2,b2,c2) =
  if disc(a1,b1,c1) = disc (a2,b2,c2) then
    let s = (Z.shift_right (Z.add b1 b2) 1) in
    let dif = Z.sub s b2 in
    let (a, x, y,z) =gcd_ext a1 a2 s in
    let (x,y,z ) = (Z.neg x,Z.neg y,Z.neg z) in
    let (c, d,g,h) = (Z.zero, Z.neg (Z.div a1 a), Z.neg (Z.div a2 a), Z.neg (Z.div s a)) in
    let (b, e, f) = ( (Z.sub  (Z.mul y dif) (Z.mul z c2)),Z.neg (Z.add (Z.mul x dif) (Z.mul z c1)), (Z.add (Z.mul x c2) (Z.mul y c1)) ) in 
    let a3 = Z.sub (Z.mul b e) (Z.mul a f)  in 
    let b3=  ( Z.sub (Z.add (Z.mul d e) (Z.mul b g)) (Z.add (Z.mul c f) (Z.mul a h))) in 
    let c3 = Z.mul d g in
    reduce (a3,Z.neg b3, c3)
  else raise Not_same_disc


let sq (a,b,c) = mult (a,b,c) (a,b,c)

  let print_t (a,b,c) = (Z.to_string a) ^"|" ^ (Z.to_string  b) ^"|"^ (Z.to_string  c)

  let print_4 (a,b,c, d) = (Z.to_string a) ^"|" ^ (Z.to_string b) ^"|"^ (Z.to_string c)^"|"^ (Z.to_string d)

  let z (a,b,c) =(Z.of_int a, Z.of_int b, Z.of_int c)