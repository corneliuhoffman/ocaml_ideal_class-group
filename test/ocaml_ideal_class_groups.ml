include Ocaml_ideal_class_groups.Ideal_class_group 

exception Invalid_string

let tuple_from_string st =
  let s = match (String.split_on_char ',' st) with
  | [a; b; c] -> (Z.of_string @@ String.trim a,Z.of_string @@ String.trim b,Z.of_string @@ String.trim c)
  | _ -> raise Invalid_string in
  s

let process_string st =
  match  (String.split_on_char '|' st) with
  | [x; y;z] ->
  (tuple_from_string x, tuple_from_string y, tuple_from_string z)
  | _ -> raise Invalid_string

let test msg f = Alcotest.test_case msg `Quick f
let test_mult =
test "mult" @@ (fun ()-> 
  let testfile = open_in "/Users/Hoff/Documents/GitLab/ocaml_ideal_class_groups/test/multiply.txt" in
 try (while true do  
  (let s = input_line testfile in
  let (a,b,c) = process_string s in
  assert (Ocaml_ideal_class_groups.Ideal_class_group.mult a b =c)) done)
with End_of_file -> close_in_noerr testfile ) 


  let () =
  Printexc.record_backtrace true ;
  Alcotest.run
    "tezos_traced_storage"
    [
      ("unit", [test_mult]);
    ]
